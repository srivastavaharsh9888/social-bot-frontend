import { Component, OnInit } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { DataService } from '../services/data.service';

declare var $: any;

@Component({
  selector: 'app-messagearea',
  templateUrl: './messagearea.component.html',
  styleUrls: ['./messagearea.component.css']
})

export class MessageareaComponent implements OnInit {
   
  socket:any;
  selected_user:any;
  
  constructor(private ds : DataService) { }
  
  ngOnInit() {
    var _this=this;
     this.socket = new WebSocket('ws://18.191.228.240:8000/ws/bot/');
      $('.submit').click(function() {
        _this.newMessage();
      });

      $(window).on('keydown', function(e) {
        if (e.which == 13) {
          _this.newMessage();
          return false;
        }
      });

   this.socket.onmessage = function(msg){
     console.log('response came '+msg.data)
     var message = JSON.parse(msg.data);
     if(message['message']['text']){
       message['message']['text'].replace(/\n/g, "<br />")
       console.log(message['message']['text'])
        $("#botmsg").append('<li class="sent" style="margin-bottom:30px;"><img src="https://3.bp.blogspot.com/-rppCsag2GpU/WGrSny3zitI/AAAAAAAAAiQ/wgOzvnL1r2wHpwrLDf4CG_xV6O_eOWerQCLcB/s1600/musicbot.png" alt="" /><div style="width:fit-content;"><pre>'+ message['message']['text'] + '</pre></div></li>')
      }
     else if(message['message']['image_url']){
        $("#botmsg").append('<div class="card sent"><img src="'+message['message']['image_url']+'" alt="Avatar" style="width:100%; height:150px;"><p><h4><b style="color:white;font-size:14px ">'+message['message']['title']+'</b></h4><p><a class="btn" href="'+message['message']['url']+'" target="_blank">Download</a></p><audio controls style="margin-top:10px; width:60%;" ><source src="'+message['message']['url']+'" type="audio/mpeg"></audio></p></div>')
      }

      else{
        $("#botmsg").append('<div class="card sent"><img src="./assets/templates/unnamed.png" alt="Avatar" style="width:100%; height:150px;"><p><h4><b style="color:white;font-size:14px">'+message['message']['title']+'</b></h4><p><a class="btn" href="'+message['message']['url']+'" target="_blank">Download</a></p><audio controls style="margin-top:10px; width:60%;"><source src="'+message['message']['url']+'" type="audio/mpeg"></audio></p></div>')
      }
   }

  }


  newMessage() {
    var  message = $(".message-input input").val();
      if($.trim(message) == '') {
        return false;
      }
      var _this=this;
      if(this.ds.login_user!=true){
      _this.socket.send(JSON.stringify({
          'message':message,
          'name':'Music Bot'
       }));
      }
      else{
          _this.ds.chat_socket.onmessage = function(msg){
           var message = JSON.parse(msg.data);
           if(message['message']['text']){
             message['message']['text'].replace(/\n/g, "<br />")
             console.log(message['message']['text'])
              $("#botmsg").append('<li class="sent" style="margin-bottom:30px;"><img src="https://3.bp.blogspot.com/-rppCsag2GpU/WGrSny3zitI/AAAAAAAAAiQ/wgOzvnL1r2wHpwrLDf4CG_xV6O_eOWerQCLcB/s1600/musicbot.png" alt="" /><div style="width:fit-content;"><pre>'+ message['message']['text'] + '</pre></div></li>')
            }
           else if(message['message']['image_url']){
              $("#botmsg").append('<div class="card sent"><img src="'+message['message']['image_url']+'" alt="Avatar" style="width:100%; height:150px;"><p><h4><b style="color:white;font-size:14px ">'+message['message']['title']+'</b></h4><p><a class="btn" href="'+message['message']['url']+'" target="_blank">Download</a></p><audio controls style="margin-top:10px; width:60%;" ><source src="'+message['message']['url']+'" type="audio/mpeg"></audio></p></div>')
            }

            else{
              $("#botmsg").append('<div class="card sent"><img src="./assets/templates/unnamed.png" alt="Avatar" style="width:100%; height:150px;"><p><h4><b style="color:white;font-size:14px">'+message['message']['title']+'</b></h4><p><a class="btn" href="'+message['message']['url']+'" target="_blank">Download</a></p><audio controls style="margin-top:10px; width:60%;"><source src="'+message['message']['url']+'" type="audio/mpeg"></audio></p></div>')
            }
         }        
        _this.ds.chat_socket.send(JSON.stringify({
          'message':message,
          'name':_this.ds.selected_user,
        }));
      }
      $('<li class="replies"><img src="http://emilcarlsson.se/assets/mikeross.png" alt="" /><p>' + message + '</p></li>').appendTo($('.messages ul'));
      $('.message-input input').val(null);
      $('.contact.active .preview').html('<span>You: </span>' + message);
    //  $(".messages").animate({ scrollTop: $(document).height() }, "fast");
    };
    
    getmusic(){
        this.newMessage();
    }
}
