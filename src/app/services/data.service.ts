import {throwError as observableThrowError, Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { Http, RequestOptions, Headers } from '@angular/http';
import { DrawerComponent } from './../drawer/drawer.component';
//import 'rxjs/add/operator/delay';
//import 'rxjs/add/observable/of';
//import 'rxjs/add/operator/catch';
//import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  login_user=false;
  chat_socket:WebSocket;
  selected_user="Music Bot";

  constructor( private http: Http) { }
  baseurl = 'http://18.191.228.240:8000/';
  Login(username: string,password: string): Observable<any> {
            const hadata = { username: username, password:password };
            return this.http.post(this.baseurl + 'Login/', hadata).pipe(
              map(res => { return res.json();
              }),
              catchError(error=> {return observableThrowError(error);}),
            );
      }
  Register(username: string,password: string): Observable<any> {
            const hadata = { username: username, password:password };
            return this.http.post(this.baseurl + 'Register/', hadata).pipe(
              map(res => { return res.json();
              }),
              catchError(error=> {return observableThrowError(error);}),
            );
      }

}
