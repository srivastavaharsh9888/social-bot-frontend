import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MessageareaComponent } from './messagearea/messagearea.component';
import { DrawerComponent } from './drawer/drawer.component';
import { DataService } from './services/data.service';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
} from "angular-6-social-login";

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("1050412538419328")
        },
        // {
        //   id: GoogleLoginProvider.PROVIDER_ID,
        //   provider: new GoogleLoginProvider("Your-Google-Client-Id")
        // },
      ]
  );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    MessageareaComponent,
    DrawerComponent
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [DataService,{
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }],
  bootstrap: [AppComponent]
})
export class AppModule {

}
