import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { SocketService } from '../services/socket.service';
declare var $: any;

import {
    AuthService,
    FacebookLoginProvider,
} from 'angular-6-social-login';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.css']
})
export class DrawerComponent implements OnInit {

  
  loginResponse:any;
  username: string;
  password: string;
  regisusername:string;
  regispassword:string;
  errMess:any;
  chatsocket:any;
  all:any;
  active_user:Array<any>;
  chat_user:string;
  current_user:any;

  constructor(private socialAuthService: AuthService,private ds: DataService) {

    
  }

  ngOnInit() {
    console.log("inside ng on iyt0");
    var _this=this;
    this.ds.selected_user="Music Bot";
    if(localStorage.getItem('logged_in')===null)
    {
      localStorage.setItem('current_user','Guest');
      this.current_user='Guest';
    }
    else{
      this.current_user=localStorage.getItem('current_user');
      this.username=this.current_user;
      this.ds.login_user=true;
      this.chatsocket = new WebSocket('ws://18.191.228.240:8000/ws/chat/'+this.username+'/');
      this.ds.chat_socket=this.chatsocket;
    }
    
    this.all = new WebSocket('ws://18.191.228.240:8000/ws/all/');
    var all=this.all;
    this.all.onopen=function(){ 
    all.send(JSON.stringify({
      'name':_this.current_user,
      }));
    }
    
    this.all.onmessage = function(msg){
        console.log(msg.data);
      _this.active_user=(JSON.parse(msg.data)).active_user;
    }
  }
  
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
      }
    );
	}
  
  loginUser() {
    var all=this.all;  
    this.ds.Login(this.username, this.password)
      .subscribe(loginres => {
        this.loginResponse = loginres;
        //localStorage.setItem('user', loginres.user_id);
        console.log(loginres);
        this.chatsocket = new WebSocket('ws://18.191.228.240:8000/ws/chat/'+this.username+'/');
        this.current_user=this.username;
        var _thistype=this;
        localStorage.setItem('logged_in','true');
        localStorage.setItem('token',loginres.token);
        localStorage.setItem('current_user',this.username);
        this.chatsocket.onopen=function(){
          _thistype.ds.login_user=true;
          _thistype.ds.chat_socket=_thistype.chatsocket;
            _thistype.all.send(JSON.stringify({
              'name':_thistype.current_user,
            }));
        }
      },
       errmess => { this.errMess = <any>errmess; }
      );
  }

  registeruser(){
    this.ds.Register(this.regisusername, this.regispassword)
      .subscribe(loginres => {

        this.loginResponse = loginres;
        localStorage.setItem('user', JSON.stringify(loginres));
        console.log('registerd in');
        console.log(loginres);
      },
      errmess => { this.errMess = <any>errmess; }
      );    
  }
  setuser(username){
  
    this.ds.selected_user=username;
    $("#botmsg").html("");
    var all_users=document.getElementsByClassName("active");
    for (let i in all_users) {  
      var user=document.getElementById(all_users[i].id);
      if(user!=null){
      user.classList.remove("active");}
    }
    var k=document.getElementById(username);
    k.classList.add("active");
    $('#userselect').html(this.ds.selected_user);
    console.log($("#"+username));
  }


  logout(){
    console.log(localStorage)
    localStorage.clear();
    document.location.reload(true);
    console.log(localStorage);
  }

}
